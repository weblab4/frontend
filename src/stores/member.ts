import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import MemberService from '@/services/member'
import type { Member } from '@/types/Member';

export const useMemberStore = defineStore('Member', () => {
  const members = ref<Member[]>([])
  const loadingStore = useLoadingStore()
  async function getMembers(id: number) {
    loadingStore.doLoad()
    const res = await MemberService.getMember(id)
    members.value = res.data
    loadingStore.finish()
  }
  async function getMember() {
    loadingStore.doLoad()
    const res = await MemberService.getMembers()
    members.value = res.data
    loadingStore.finish()
  }
  async function saveMember(Member: Member) {
    loadingStore.doLoad()
    if (Member.id < 0) {
      const res = await MemberService.addMember(Member)
    } else {
      const res = await MemberService.updateMember(Member)
    }
    await getMember()
    loadingStore.finish()
  }
  async function deleteMember(Member: Member) {
    loadingStore.doLoad()
    const res = await MemberService.delMember(Member)
    await getMember()
    loadingStore.finish()
  }
  return { members, getMember, saveMember, deleteMember, getMembers }
})
